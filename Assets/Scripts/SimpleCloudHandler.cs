﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.Video;
using UnityEngine.UI;

public class SimpleCloudHandler : MonoBehaviour, ICloudRecoEventHandler {


	private CloudRecoBehaviour mCloudRecoBehaviour;
	private bool mIsScanning = false;
	private string mTargetMetadata = "";
	public ImageTargetBehaviour ImageTargetTemplate;
	public GameObject internetConnectionBar;
	public GameObject internetConnectionBar1;
	//

	public GameObject player;

	//

	public VideoClip videoToPlay;

	public VideoPlayer videoPlayer;
	public VideoSource videoSource;

	public AudioSource audioSource;


	public Text _text;

	public GameObject scanningButton;

	// Use this for initialization
	void Start () {

		if (Application.internetReachability == NetworkReachability.NotReachable) {
			Debug.Log ("No internet connection");
			internetConnectionBar.SetActive (true);
		}
		// register this event handler at the cloud reco behaviour
		else {

			internetConnectionBar.SetActive (false);
			mCloudRecoBehaviour = GetComponent<CloudRecoBehaviour> ();

			if (mCloudRecoBehaviour) {
				mCloudRecoBehaviour.RegisterEventHandler (this);
			}
		}
	}

	public void Update()
	{
		//Debug.Log ("Calling update");
		if (Application.internetReachability == NetworkReachability.NotReachable) {
			internetConnectionBar1.SetActive (true);

		} else {
			internetConnectionBar1.SetActive (false);
			internetConnectionBar.SetActive (false);
		}
	}



	public void OnInitialized() {
		Debug.Log ("Cloud Reco initialized");
	}
	public void OnInitError(TargetFinder.InitState initError) {
		Debug.Log ("Cloud Reco init error " + initError.ToString());
	}
	public void OnUpdateError(TargetFinder.UpdateState updateError) {
		Debug.Log ("Cloud Reco update error " + updateError.ToString());
	}


	public void OnStateChanged(bool scanning) {
		mIsScanning = scanning;
		if (scanning)
		{
			// clear all known trackables
			var tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
			tracker.TargetFinder.ClearTrackables(false);
		}
	}


	// Here we handle a cloud target recognition event
	public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult) {

		// Build augmentation based on target
		if (ImageTargetTemplate) {
			// enable the new result with the same ImageTargetBehaviour:
			ObjectTracker tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
			ImageTargetBehaviour imageTargetBehaviour =
				(ImageTargetBehaviour)tracker.TargetFinder.EnableTracking(
					targetSearchResult, ImageTargetTemplate.gameObject);
		}


//		// stop the target finder (i.e. stop scanning the cloud)
		mCloudRecoBehaviour.CloudRecoEnabled = false;

		string url = "http://www.whitethoughtsdigital.com/dev/cloudarapp/getAssetBundleTarget.php";

		string id = targetSearchResult.UniqueTargetId;

		_text.text = "Image Detected...";


		Debug.Log (",,,,,,,,,,"+id);

		WWWForm form = new WWWForm();

		form.AddField ("targetID",id);

		WWW www = new WWW (url, form);

		StartCoroutine (WaitForRequest(www));

	}


	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;

		// check for errors
		if (www.error == null)
		{
			Debug.Log ("enter,,,,");
			string imageName = www.text;

			Debug.Log (".............."+imageName);

			string _url = "https://d33qpa0ixpc550.cloudfront.net/"+imageName;

			_text.text = "Downloading Video..";

			Debug.Log ("URL= "+_url);

			// Vide clip from Url
			videoPlayer.source = VideoSource.Url;

			videoPlayer.url = _url;

			//Set Audio Output to AudioSource
			videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

			//Assign the Audio from Video to AudioSource to be played
			videoPlayer.EnableAudioTrack(0, true);
			videoPlayer.SetTargetAudioSource(0, audioSource);

			//Set video To Play then prepare Audio to prevent Buffering
//			videoPlayer.clip = videoToPlay;
			videoPlayer.Prepare();

//			videoPlayer.prepareCompleted+ =

			_text.text = "Preparing Video..";

			//Wait until video is prepared
			while (!videoPlayer.isPrepared)
			{
				yield return null;
			}


			Debug.Log("Done Preparing Video");

			videoPlayer.Play();

			audioSource.Play();

			Debug.Log("Playing Video");

			_text.text = "Playing Video..";


		} 

		else
		{
			
		}    
	} 



	void OnGUI() {
//		// Display current 'scanning' status
//		GUI.Box (new Rect(100,100,200,50), mIsScanning ? "Scanning"+mIsScanning : "Not scanning"+mIsScanning);
//		// Display metadata of latest detected cloud-target
//		GUI.Box (new Rect(100,200,200,50), "Metadata: " + mTargetMetadata);
		// If not scanning, show button
		// so that user can restart cloud scanning


		if (mIsScanning)
		{
			_text.text = "Scanning...";
		}
//		else
//		{
//			_text.text = "NOT SCANNING..";
//
//		}


		if (!mIsScanning) {
			scanningButton.SetActive (true);

		} 
		else
		{
			scanningButton.SetActive (false);

		}
	}


	public void OnRestart()
	{
		OnTrackingLost ();
		videoPlayer.Stop ();
		mCloudRecoBehaviour.CloudRecoEnabled = true;	

		scanningButton.SetActive (false);

	}


	public void OnTrackingLost()
	{
		player.GetComponent<Renderer> ().enabled = false;

	}


}
