﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Video; 

public class Demo : MonoBehaviour {


	public GameObject VideoPlayerGO;

	VideoPlayer videoPlayer;

	VideoSource _videoSource;

	AudioClip _audio;

	IEnumerator testVideoDownload()
	{
//		var www = new WWW ("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");

		var www = new WWW ("https://d33qpa0ixpc550.cloudfront.net/wtb_mashup_whatsapp.mp4");

		Debug.Log ("Downloading video..");

		yield return www;

		File.WriteAllBytes(Application.persistentDataPath+"/"+"Arvideo.mp4",www.bytes);

		Debug.Log ("File Saved.....");

		PlayVideo("Arvideo.mp4");

	}


	void Awake()
	{
		videoPlayer=VideoPlayerGO.GetComponent<VideoPlayer>();
	}
	void Start()
	{

		StartCoroutine (testVideoDownload());

	}
	void PlayVideo(string fileName)
	{
		videoPlayer.url= Application.persistentDataPath +"/"+ fileName;
		videoPlayer.Play();
	}
		
}
