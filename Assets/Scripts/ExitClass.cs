﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitClass : MonoBehaviour {

	// Use this for initialization

	
//	// Update is called once per frame
//	void Update() {
//		if (Input.GetKey("escape"))
//			Application.Quit();
//
//	}
	public void OnQuit() {
		Application.Quit();
	}

	public void OnMenu()
	{
		SceneManager.LoadScene ("MainUI");
	}
}
